# 1U Rack Pi4 Insert with Fan Shroud

[Check out the 3D model previews](https://rack-pi4-case-servergnome-prints-0dd5b10526d1d896770a8dc80da83b.gitlab.io/)

If you like this model, please consider [buying me a coffee](https://ko-fi.com/horza).

Printable Raspberry Pi 4 insert for 1U 19' rack with fan shroud.
Compatible with [1U Raspberry Pi Rack with Moduler trays](https://www.printables.com/model/69176-1u-raspberry-pi-rack-with-moduler-trays) by [Robert (@Robert_28442 at Printables)](https://www.printables.com/@Robert_28442)
Based on Robert's Pi4 insert, but with a fan shroud and a few ventilation hole tweaks.
Compatible with v1 and v2 of the rack.

## Description

This is a modified Raspberry Pi 4 insert for 1U 19' rack.

It is compatible with [1U Raspberry Pi Rack with Moduler trays](https://www.printables.com/model/69176-1u-raspberry-pi-rack-with-moduler-trays) by [Robert (@Robert_28442 at Printables)](https://www.printables.com/@Robert_28442).
Modele is based on Robert's Pi4 basic insert (v2), but extended to support a fan shroud.

Enables mounting a 40mm fan on top of the Pi4 to keep it cool.
Fan is horizontal to clear the standard rack insert slot, but also tilted backwards to minimize bouncing air all over the place and promote air movement front to back.
The shroud is designed to take the cool air through the insert cutouts, drag it across components that like to heat up and finally through the CPU heatsink before exhausting it up and backwards.

![Model Preview](preview/model.png){width=33%}
![In Rack](preview/in-rack.jpg){width=33%}

## Usage

Slice the STP file yourself please. Included STP and 3mf files can be opened in almost any slicer, but please don't print 3mf files directly. They are tuned for Prusa Mini and may not work well with other printers.
You'll want to use ~M3x8 screws to attach the shroud and ~M3x12 to attach the fan. Threads are not modeled so you'll want to either tap them or use metal screws to cut into the plastic. I didn't see any need to use the front 2 holes on the Raspberry Pi at all because the insert, Raspberry Pi and shroud are quite snug with just two rear screws.

## Support

This is provided as-is and there's no support. Instead, there is a FreeCAD source file and STP file to make it easier to modify while respecting upstream licenses.

## Contributing

Merge requests are always welcome.
The FreeCAD file is made with v0.22. Please note that it is not backward compatible with older versions.

## Authors and acknowledgment

[Robert (@Robert_28442 at Printables)](https://www.printables.com/@Robert_28442) designed the original Raspberry Pi 4 insert for 1U 19' rack. I just added an extension, fan shroud and a few ventilation holes.
[Hasanain Shuja](https://grabcad.com/hasanain.shuja-1) published the [Raspberry Pi 4 model on GrabCAD](https://grabcad.com/library/raspberry-pi-4-model-b-1).

## License

This project is licensed under the terms of the Creative Commons - Attribution - ShareAlike license in order to comply with Robert's [LICENSE](LICENSE). As far as my work on it is concerned, you can do whatever you want with it as long as you give credit to Robert.
