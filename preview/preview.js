document.addEventListener('DOMContentLoaded', function() {
    // Get all images within .item
    var images = document.querySelectorAll('.thumb img');

    // Add a click event listener to each image
    images.forEach(function(img) {
        img.addEventListener('click', function() {
            // Toggle the .zoomed class on click
            this.classList.toggle('zoomed');
        });
    });
});
